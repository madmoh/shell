#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

//
// Data structures
//
struct command {
    int number;
    string entry;
};

struct job {
    int PID;
    string Command;
};

//
// Macros
//
#define numel(x) (sizeof(x) / sizeof((x)[0]))

//
// Function declarations
//
bool shell_jobs(vector<string> arguments);
bool shell_cd(vector<string> arguments);
bool shell_history(vector<string> arguments);
bool shell_exit(vector<string> arguments);
bool shell_kill(vector<string> arguments);
bool shell_setenv(vector<string> arguments);
bool shell_printenv(vector<string> arguments);
bool shell_repeat(vector<string> arguments);

void storeCommand(const string entry);
void parseCommand(const string &command, vector<string> &arguments);
bool executeAnyCommand(vector<string> &arguments);
bool isBackgroundJob(vector<string> &arguments);
void removeBackgroundJob(int pid);

//
// Global variables
//
const string builtInCommands[] = {
    "jobs",
    "cd",
    "history",
    "exit",
    "kill",
    "setenv",
    "printenv"};

// Syntax learnt from https://github.com/brenns10/lsh
bool (*builtInFunctions[])(vector<string> arguments) = {
    &shell_jobs,
    &shell_cd,
    &shell_history,
    &shell_exit,
    &shell_kill,
    &shell_setenv,
    &shell_printenv,
    &shell_repeat};

const int COMMANDS_MAX = 5;
vector<command> history;
vector<job> backgroundJobs;

//
// Built-in commands
// Will return true if argument matches any of of the built-in
//   commands, or if it starts with "!"
//
bool shell_jobs(vector<string> arguments) {
    if (backgroundJobs.empty()) {
        cout << "No Background Jobs" << endl;
        return true;
    } else {
        for (int i = 0; i < backgroundJobs.size(); i++) {
            cout << "[" << backgroundJobs[i].PID << "]: " << backgroundJobs[i].Command << endl;
        }
    }

    return true;
}

bool shell_cd(vector<string> arguments) {
    if (arguments.size() != 2) {
        cout << "shell: no argument specified" << endl;
    } else {
        if (chdir(arguments[1].c_str()) != 0) {
            cout << "shell: changing directory error" << endl;
        }
    }
    return true;
}

bool shell_history(vector<string> arguments) {
    if (history.size() > 0) {
        cout << "Number\tCommand" << endl;
    }
    for (int i = 0; i < history.size(); i++) {
        cout << history[i].number << "\t" << history[i].entry << endl;
    }
    return true;
}

bool shell_exit(vector<string> arguments) {
    vector<string> blank;
    if (backgroundJobs.size() == 0) {
        exit(EXIT_SUCCESS);
        return false;  // terminate execution
    } else {
        cout << "The following background processes are preventing exit:\n";
        shell_jobs(blank);
        return true;
    }
}

bool shell_kill(vector<string> arguments) {
    for (int i = 1; i < arguments.size(); i++) {
        for (int j = 0; j < backgroundJobs.size(); j++) {
            if (stoi(arguments[i]) == backgroundJobs[j].PID) {
                kill(backgroundJobs[j].PID, SIGKILL);
                removeBackgroundJob(backgroundJobs[j].PID);
            }
        }
    }
    return true;
}

bool shell_setenv(vector<string> arguments) {
    if (arguments.size() != 3) {
        cout << "shell: syntax error" << endl;
    } else {
        setenv(arguments[1].c_str(), arguments[2].c_str(), 1);
    }

    return true;
}

bool shell_printenv(vector<string> arguments) {
    if (arguments.size() < 2) {
        cout << "shell: syntax error" << endl;
        return true;
    }
    if (arguments[1][0] != '$') {
        cout << "shell: syntax error" << endl;
        return true;
    }
    string name = arguments[1].substr(1);
    const char *val = getenv(name.c_str());
    string valstr = val ? val : "";
    if (valstr.empty()) {
        cout << "shell: " << name << " is not an environment variable" << endl;
    } else {
        cout << valstr << endl;
    }

    return true;
}

bool shell_repeat(vector<string> arguments) {
    int number = -1 * COMMANDS_MAX;
    try {
        number = stoi(arguments[0].substr(1));
    } catch (invalid_argument const &e) {
        cout << "Bad input: std::invalid_argument thrown" << endl;
    } catch (out_of_range const &e) {
        cout << "Integer overflow: std::out_of_range thrown" << endl;
    }

    if (number < 0 &&
        number >= -1 * COMMANDS_MAX) {
        number = history[history.size() + number].number;
    }

    if (number < history[0].number ||
        number > history[history.size() - 1].number) {
        cout << "shell: command not in history" << endl;

    } else {
        vector<string> command_arguments;
        storeCommand(history[number - history[0].number].entry);
        parseCommand(history[number - history[0].number].entry, command_arguments);
        executeAnyCommand(command_arguments);
    }

    return true;
}

// If has STDIN, remove the "<" argument, and set the child's
// STDIN to the specified file
void redirectIn(vector<string> &arguments) {
    for (int i = 0; i < arguments.size(); i++) {
        if (arguments[i] == "<") {
            if (arguments.size() > i + 1) {
                int fdin;
                if ((fdin = open(arguments[i + 1].c_str(), O_RDONLY)) == -1) {
                    cout << "shell: can't locate file " << arguments[i + 1] << endl;
                }
                dup2(fdin, STDIN_FILENO);
                close(fdin);

            } else {
                cout << "shell: no infile specified" << endl;
            }

            arguments.erase(arguments.begin() + i, arguments.begin() + i + 2);
            break;
        }
    }
}

// If has STDOUT, remove the ">" argument, and set the child's
// STDOUT to the specified file
void redirectOut(vector<string> &arguments) {
    for (int i = 0; i < arguments.size(); i++) {
        if (arguments[i] == ">") {
            if (arguments.size() > i + 1) {
                int fdout;
                if ((fdout = open(arguments[i + 1].c_str(), O_WRONLY | O_TRUNC | O_CREAT, 0666)) == -1) {
                    cout << "shell: can't locate file " << arguments[i + 1] << endl;
                }
                dup2(fdout, STDOUT_FILENO);
                close(fdout);

            } else {
                cout << "shell: no outfile specified" << endl;
            }

            arguments.erase(arguments.begin() + i, arguments.begin() + i + 2);
            break;
        }
    }
}

bool isBuiltInCommand(string &argument) {
    for (int i = 0; i < numel(builtInCommands); i++) {
        if (argument == builtInCommands[i]) {
            return true;
        }
    }
    if (argument[0] == '!') {
        return true;
    }
    return false;
}

bool executeBuiltInCommand(vector<string> &arguments) {
    bool builtInReturn = true;
    for (int i = 0; i < numel(builtInCommands); i++) {
        if (arguments[0] == builtInCommands[i]) {
            builtInReturn = builtInFunctions[i](arguments);
        }
    }
    if (arguments[0][0] == '!') {
        builtInReturn = builtInFunctions[numel(builtInFunctions) - 1](arguments);
    }
    return builtInReturn;
}

// STDIN and STDOUT from http://www.cs.loyola.edu/~jglenn/702/S2005/Examples/dup2.html
// Creating char** from vector<string> https://stackoverflow.com/a/54519338
bool executeCommand(vector<string> &arguments) {
    if (isBackgroundJob(arguments)) {
        // remove last argument ("&")
        arguments.erase(arguments.begin() + arguments.size() - 1);
    }

    redirectIn(arguments);

    // Interpret the arguments as char**
    std::vector<char *> argc;
    for (auto const &a : arguments) {
#if defined(__linux__)
        argc.emplace_back(const_cast<char *>(a.c_str()));
#elif defined(__APPLE__) && defined(__MACH__)
        argc.__emplace_back(const_cast<char *>(a.c_str()));
#endif
    }
    argc.push_back(nullptr);

    // Execute the command
    int execvpRes = execvp(arguments[0].c_str(), argc.data());

    if (execvpRes == -1) {
        cout << "shell: " << arguments[0] << " command was not found" << endl;
    }

    return execvpRes;
}

void printPrompt() {
    // Get the current working directory
    char cwdBuffer[PATH_MAX];
    getcwd(cwdBuffer, sizeof(cwdBuffer));

    // Print the prompt
    cout << "\033[1;31m"  // Start bold red text
         << cwdBuffer
         << "\033[0m"  // End bold red text
         << "> ";
}

void parseCommand(const string &command, vector<string> &arguments) {
    string argument;

    istringstream tokenStream(command);
    while (getline(tokenStream, argument, ' ')) {
        arguments.push_back(argument);
    }

    for (int i = 0; i < arguments.size(); i++) {
        if (arguments[i][0] == '$') {
            string name = arguments[1];
            const char *val = getenv(name.c_str());
            string valstr = val ? val : "";
            if (!valstr.empty()) {
                arguments[i] = valstr;
                break;
            }
        }
    }
}

void storeCommand(const string entry) {
    // Check if a command that should not be stored
    if (entry[0] == '!' ||
        entry == "history" ||
        entry == "") {
        return;
    }

    command cmnd;
    cmnd.entry = entry;

    if (history.size() == 0) {
        cmnd.number = 1;
        history.push_back(cmnd);

    } else {
        if (history.size() >= COMMANDS_MAX) {
            history.erase(history.begin());
        }

        cmnd.number = history[history.size() - 1].number + 1;
        history.push_back(cmnd);
    }
}

void ctrl_c_handler(int sig) {
    cout << "ctrl+c interrupt" << endl;
}

bool executeAnyCommand(vector<string> &arguments) {
    // If no command, ignore
    if (arguments.size() == 0) {
        return true;
    }

    // If a built-in command, go to built-in command execution function
    if (isBuiltInCommand(arguments[0])) {
        executeBuiltInCommand(arguments);

    } else {
        int status;
        pid_t childPid = fork();

        if (childPid == 0) {
            if (executeCommand(arguments) == -1) {
                cout << "shell: execution error" << endl;
            }
            exit(EXIT_FAILURE);

        } else if (childPid < 0) {
            cout << "shell: forking error" << endl;

        } else {
            if (isBackgroundJob(arguments)) {
                // Add to the vector of background job
                job backgroundJob;
                backgroundJob.PID = childPid;
                backgroundJob.Command = arguments[0];
                backgroundJobs.push_back(backgroundJob);

            } else {
                waitpid(childPid, &status, WUNTRACED);
            }
        }
    }
    return true;
}

bool isBackgroundJob(vector<string> &arguments) {
    return arguments[arguments.size() - 1] == "&";
}

void removeBackgroundJob(int pid) {
    for (int i = 0; i < backgroundJobs.size(); i++) {
        if (backgroundJobs[i].PID == pid) {
            backgroundJobs.erase(backgroundJobs.begin() + i);
            break;
        }
    }
}

void checkBackgroundProcesses() {
    int status;
    for (int i = 0; i < backgroundJobs.size(); i++) {
        waitpid(backgroundJobs[i].PID, &status, WNOHANG);
        if (status == 0) {
            removeBackgroundJob(backgroundJobs[i].PID);
            i--;
        }
    }
}

//
// GOTO: basic shell
//
int main() {
    string command;
    vector<string> arguments;
    bool status = true;
    signal(SIGINT, ctrl_c_handler);

    while (status) {
        printPrompt();

        checkBackgroundProcesses();
        getline(cin, command);

        storeCommand(command);
        parseCommand(command, arguments);

        int fdtemp = dup(STDOUT_FILENO);
        redirectOut(arguments);

        status = executeAnyCommand(arguments);

        dup2(fdtemp, STDOUT_FILENO);

        arguments.clear();
    }
    return EXIT_SUCCESS;
}